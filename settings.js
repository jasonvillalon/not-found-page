module.exports = {
  Name: "PageNotFound",
  Description: "Page not found",
  Repository: "git@bitbucket.org:jasonvillalon/not-found-page.git",
  AtomicDeps: [
    {
      "Name": "Layout",
      "Description": "Layout",
      "Author": "Jason Villalon",
      "Repository": "git@bitbucket.org:generator-react-component/ac-layout.git"
    }
  ]
}
